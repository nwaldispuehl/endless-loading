package ch.retorte.endlessloading;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpServerRequest;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Entry point for the endless loading test app.
 */
public class EndlessLoading extends AbstractVerticle {

  //---- Statics

  private static final int PORT = 8080;
  private static final long THOUSAND = 1000;

  private static final int TIMEOUT_MIN = 0;
  private static final int TIMEOUT_MAX = 600;

  private static final String TIMEOUT_PARAMETER = "timeout_seconds";
  private static final String STATUS_PARAMETER = "status";

  private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");


  //---- Methods

  @Override
  public void start() {
    vertx.createHttpServer().requestHandler(this::handle).listen(PORT);
    log("0.0.0.0", "Endless Loading started.");
  }

  private void handle(HttpServerRequest request) {
    String visitor = getVisitorHostFrom(request);

    Long timeoutSeconds = null;
    int status = 200;
    try {
      String timeoutParameter = request.getParam(TIMEOUT_PARAMETER);
      String statusParameter = request.getParam(STATUS_PARAMETER);
      log(visitor, String.format("New request received with arguments: timeout_seconds=%s, status=%s", timeoutParameter, statusParameter));

      if (timeoutParameter != null) {
        timeoutSeconds = Long.parseLong(timeoutParameter);
      }

      if (statusParameter != null) {
        status = Integer.parseInt(statusParameter);
      }
    } catch (Exception e) {
      log(visitor, "Invalid parameter: " + e.getMessage());
    }

    if (timeoutSeconds != null && isInRange(timeoutSeconds)) {
      startTimeoutWith(request, timeoutSeconds, status);
    } else {
      log(visitor, "No valid timeout parameter received. Sending help message.");
      request.response().end(helpMessage());
    }
  }

  private boolean isInRange(long seconds) {
    return TIMEOUT_MIN <= seconds && seconds <= TIMEOUT_MAX;
  }

  private void startTimeoutWith(HttpServerRequest request, long seconds, int status) {
    String visitor = getVisitorHostFrom(request);
    log(visitor, String.format("Starting timeout with %d seconds and status %d", seconds, status));

    vertx.setTimer(seconds * THOUSAND, timerID -> {
      String responseString = String.format("Ok. Waited %d seconds.", seconds);
      log(visitor, responseString);
      request.response().setStatusCode(status).end(responseString + "\n");
    });
  }

  private String helpMessage() {
    return String.format("<html>Endless loading. Use 'timeout_seconds' parameter to specify the timeout in seconds (range: [%d, %d]), and optionally `status` to set a custom status (e.g. `500`, default: `200`) " +
        "e.g. <a href='?timeout_seconds=5'>HOST/?timeout_seconds=5</a></html>", TIMEOUT_MIN, TIMEOUT_MAX);
  }

  private void log(String sessionId, String message) {
    System.out.println(LocalDateTime.now().format(DATE_TIME_FORMATTER) + " - " + sessionId + " - " + message);
  }

  private String getVisitorHostFrom(HttpServerRequest request) {
    String xForwardedForHeader = request.getHeader("X-Forwarded-For");
    return xForwardedForHeader != null ? xForwardedForHeader : request.remoteAddress().toString();
  }

}
