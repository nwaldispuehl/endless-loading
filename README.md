# Endless Loading Testing App

Web server which just loads for a given amount of time, e.g. 20 seconds. Can be used to test timeouts.

Build with:

    $ ./gradlew shadowjar

Create image with:

    $ docker build -t endless-loading .

Create image tagged for registry with:

    $ docker build -t registry.gitlab.com/nwaldispuehl/endless-loading .

Push image

    $ docker push registry.gitlab.com/nwaldispuehl/endless-loading

Launch container with e.g.

    $ docker run -d -p 8080:8080 --restart unless-stopped --name endless-loading registry.gitlab.com/nwaldispuehl/endless-loading
